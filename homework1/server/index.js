const express = require("express");
const fileRouter = require("./routes/file.routes");
const globalRouter = require("./routes/global.routes");

const app = express();

app.use(express.json());
app.use("/api/files/", fileRouter);
app.use("/", globalRouter);

app.get("/", (req, res) => {
  res.status(200).json({ message: "Start page here" });
});

const PORT = 8080;

app.listen(PORT, () => {
  console.log(`Server listening at http://localhost:${PORT}`);
});
