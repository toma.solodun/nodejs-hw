const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../../swaggerDefinition");
const express = require("express");
const urlRoutes = express.Router();

const controller = require("../controllers/file.controller");

urlRoutes.post("/", controller.createOneRequest);
urlRoutes.get("/:filename", controller.readOneRequest);
urlRoutes.get("/", controller.readAllRequest);
urlRoutes.put("/:filename", controller.updateOneRequest);
urlRoutes.delete("/:filename", controller.deleteOneRequest);

module.exports = urlRoutes;
