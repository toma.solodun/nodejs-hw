const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../../swaggerDefinition");
const express = require("express");
const globalRoutes = express.Router();

globalRoutes.use("/api-docs", swaggerUi.serve);
globalRoutes.get("/api-docs", swaggerUi.setup(swaggerDocument));

module.exports = globalRoutes;
